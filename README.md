# JAFS

> Just Another Furious Shooter

J.A.F.S. ist ein "grösseres" Java-Programmierprojekt der 4 Studierenden Julian, Alexander, Fabio und Serzer. Hintergrund hierfür ist die Vorlesung "Programmieren II", in der ein Computerspiel in Client/Server-Architektur konzipiert und implementiert werden soll.

Wir habe uns für einen Echtzeit Shooter aus der Vogelperspektive entschieden, angeleht an einen sehr bekannten Ego-Shooter.